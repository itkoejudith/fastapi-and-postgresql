from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from queries.vacations import (
    Error,
    VacationIn,
    VacationRepository,
    VacationOut,
)

router = APIRouter()


@router.post("/vacations", response_model=Union[VacationOut, Error])
def create_vacation(
    vacation: VacationIn, response: Response, repo: VacationRepository = Depends()
):
    response.status_code = 400
    return repo.create(vacation)


@router.get("/vacations", response_model=Union[List[VacationOut], Error])
def get_all(
    repo: VacationRepository = Depends(),
):
    return repo.get_all()


@router.put("/vacations/{vacation_id}", response_model=Union[VacationOut, Error])
def update_vacation(
    vacation_id: int,
    vacation: VacationIn,
    repo: VacationRepository = Depends(),
) -> Union[Error, VacationOut]:
    return repo.update(vacation_id, vacation)


@router.delete("/vacations/{vacation_id}", response_model=bool)
def delete_vacation(
    vacation_id: int,
    repo: VacationRepository = Depends(),
) -> bool:
    return repo.delete(vacation_id)


@router.get("/vacations/{vacation_id}", response_model=Optional[VacationOut])
def get_one_vacation(
    vacation_id: int,
    response: Response,
    repo: VacationRepository = Depends(),
) -> VacationOut:
    vacation = repo.get_one(vacation_id)
    if vacation is None:
        response.status_code = 404
    return vacation


# VacationIn class in queries, pydantic base model -nothing to do with database
# fastapi, send it to api, take the json, and turn it into object (encoders)
# if not used all of the stuff on basemodel,
# and field is missing bc it was supposed to be required
# if no to_date error 422 unprocessable entity field required, but is missing
# getting data into endpoints
# capture data that we want from our endpoint

# when adding response-model what kind of thing actually gets created
# vacation out
# if valid data, then you get data back in fastapi, id name, from,to ,thoughts,
#  successful response looks like
# response model can be trouble, value that we send back return {"message": "error!"}
# we end up w 500 internal server error
# we get lots of output, validation errors, 4 validation errors for vaca out
# expects properties and only has message and on terminal it will show that you need id,name..and so on.
# responsemodel-doing validation, data sending back exact number of properties and if not sending all of them it will give a 500 error
# couple things to do, check what data is missing, for example we add vacation out expects rating
# expect that and when its runned it will give 500 internal server error
# validation error rating was required
# if we made error model you can identify what you want and
# response above with union if sending something back and has all properties, or not there you get a response
# response model good to use and see it properly
# set response to status  more than one response model instead of getting 200 response,
# something doesnt meet it will validate all properties you expect, if not on there wonky error.
# build react frontend and use it in the ui
