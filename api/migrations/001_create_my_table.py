# always start with 000,

steps = [
    [
        ##create the table
        """
        CREATE TABLE vacations (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            from_date DATE NOT NULL,
            to_date DATE NOT NULL,
            thoughts TEXT
        );
        """,
        ##drop the table
        """
        DROP TABLE vacations;
        """,
    ]
]

# table about vacations,
# create an id we need to make it everytime, serial number keeps getting bigger,
# autopopulate also, primary key to identify each row in our table.
# name of vacation, has to have value (not null) varchar only 1000 characters
# thoughts about vacation text, it can be any amount of text x amount you want,
# date is from and to of vacation postgresql.org search for datatypes, documentation chapter 8
# date/datetime types
# we put a comma on line 14 bc this is a list.
# capital on 4 and 15 stay consistent on uppercase or lowercase
# up is building down is destroying table
# to check container running : docker ps
# docker exec -it example-db-1- bash
# psql  -h localhost -U example_user example  FROM THE DEV YML FILE
# no root bc no username root use the one database created
# \d
# shows list of everything table created for us
# select count(*) from migrations;
# nothing in there bc we just created 001 migrations file, so connect to example-api-1 of container
# docker exec -it example-api-1 bash
# app# ls
# app# python -m migrations up
# if terminal doesnt say anythign its good
# if select * from migrations;
# then you will see folder and table
# python -m migrations down drop table
# vacations gone from table

# build up database as you do development
# vacations table up and everything specified from the table
# small migrations library to set up project create migrations folder, init.py, and main.py and copy from here to see table
# now that we have a table lets put stuff in
