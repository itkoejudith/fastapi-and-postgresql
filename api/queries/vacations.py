from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


# for our endpoints not database
class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationRepository:
    def get_one(self, vacation_id: int) -> Optional[VacationOut]:
        try:
            with pool.connection() as db:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , from_date
                            , to_date
                            , thoughts
                        FROM vacations
                        WHERE id = %s
                        """,
                        [vacation_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_vacation_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that specific vacation"}

    def delete(self, vacation_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM vacations
                        WHERE id = %s
                        """,
                        [vacation_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(
        self, vacation_id: int, vacation: VacationIn
    ) -> Union[VacationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE vacations
                        SET name = $s
                            , from_date = %s
                            , to_date = %s
                            , thoughts = %s
                        WHERE id = %s
                        """,
                        [
                            vacation.name,
                            vacation.from_date,
                            vacation.to_date,
                            vacation.thoughts,
                            vacation_id,
                        ],
                    )
                    # old_data = vacation.dict()
                    # return VacationOut(id=vacation_id, **old_data)
                    return self.vacataion_in_to_out(vacation_id, vacation)
        except Exception as e:
            print(e)
            return {"message": "Could not get that specific vacation"}

    def get_all(self) -> Union[Error, List[VacationOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, from_date, to_date, thoughts
                        FROM vacations
                        ORDER BY from_date;
                        """
                    )
                    # result = []
                    # for record in db:
                    #     vacation = VacationOut(
                    #         id=record[0],
                    #         name=record[1],
                    #         from_date=record[2],
                    #         to_date=record[3],
                    #         thoughts=record[4],
                    #     )
                    #     result.append(vacation)
                    # return result
                    return [
                        VacationOut(
                            id=record[0],
                            name=record[1],
                            from_date=record[2],
                            to_date=record[3],
                            thoughts=record[4],
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all vacations"}

    def create(self, vacation: VacationIn) -> VacationOut:
        # connect the database
        with pool.connection() as conn:
            # get a cursor (run sql with)
            with conn.cursor() as db:
                # run our insert statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, to_date, thoughts)
                    VALUES
                        (%s, %s, %s, %s);
                    RETURNING id;
                    """,
                    [
                        vacation.name,
                        vacation.from_date,
                        vacation.to_date,
                        vacation.thoughts,
                    ],
                )
                id = result.fetchone()[0]
                # return new data
                # old_data = vacation.dict()
                # return VacationOut(id=id, **old_data)
                return self.vacataion_in_to_out(id, vacation)

    def vacation_in_to_out(self, id: int, vacation: VacationIn):
        old_data = vacation.dict()
        return VacationOut(id=id, **old_data)

    def record_t0

# endpoint create vacation, type vacation in,
# connect to db, mongo db will make pool of connections, and you can reuse them if you want.
# create a pool.py file in queries, and make a pool connection, inside docker yml database_url use that environment variable
# import os on top of pool.py, (conninfo=os.environ["DATABASE_URL"]) POOL OBJECT TO GET connections to the database
# connect the pool module to pool.py
# cursor-run sql with
# run our insert statement from queries.pool file and import pool variable
#
# return new data and fastapi will return an id
# psycopg library search on google, and look at doc link, and look at how to get started
# when doing execute is inserting table,
# multi-line sql statements
# vacations table, id will have its own value, need name, from date, to date and thoughts
# specify values that you want to insert values
# currently those values are inside vacation objects sooooo
# 4 values, so do 4 %s variable placeholders
# second parameter to execute is the list vacation.name,...
# insert statement and insert these 4 columns, and insert 4 values for these rows
# return id; at the end of insert statement, capture id as a result
# import vaca repository and do the dependency injection
# created vacation on object on terminal and
# pysocpg.cursor - go to doc and try fetchone (2,) tuples will show the 2 vacations
# create new model vacation out
# id = result fetchone [0]
# old data coming from vacation, dct on it pydantic models in, out , has dict property
# give back dict info, so when we do vacation.dict and id = id ** old data we get everything back
# typeins  returns vacation out, except vacation in and out, BC OF THE ENDPOINT RETURN REPO.CREATE(VACATION)
# fastapi returns id 3, then 4 eveytime we execute
# REPO.CREATE WHICH INSIDE THE REPOSITORY DEF, TAKES DATA RECEIVED,
# PUTS INTO DB USING SQL STATEMENT, THAT HAD GOTTEN A CONNECTION,
# GETS A CURSOR, EXECUTES SQL, GETS SOME RESULT, THEN WITH THAT RESULT
# WE FETCH SOMETHING OUT OF THAT RESULT, FETCHING ONE BC THERE IS ONLY
#  ONE RESULT AND FIRST ENTRY IS THAT ID, RETURNING ID, IF WE WANTED MORE,
#  WE WOULD ADD TO RETUN INSQL STATEMENT RETURN DATA IN DICT AND SPLAT
#  DATA OUT
# TAKES ALL REPO. COMES FROM VACATION REPOSITORY
# in repository get all, there is a map, and filter throught that loop


## UPDATE
# MADE ROUTER ID IN PATH, GETTING DATA IN AND USING VACATION REPOSITORY JUST TO UPDATE,
# SAME PATTERN IN DEF UPDATE, TRY CATCH, DB DIES, OUR APP DOENST DIE, GET CONNECTION, GET CURSOR EXECUTE STATEMENT, PASS IN VALUES
# UPDATE STATEMENT WE WANT TO USE %S AND PASS THEM IN ORDER, AND THEN RETURN VACATION OUT
# REFACTOR TWO LINES OF OLD DATA INTO TO LINES SO ITS USED IN MORE THAN ONE PLACE
#
