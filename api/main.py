from fastapi import FastAPI
from routers import vacations

app = FastAPI()
app.include_router(vacations.router)

# under api, create a router directory routers-endpoints fastapi vacations.py for specific things and also
# queries directory, code to connect to sql to connect to database inside queries vacations.py query for database
# to separated files from main.py
# we want organized code


# create app, and include routers for endpoints, router specifically for vacations.
